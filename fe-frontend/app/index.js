'use strict';

require('assets/scss/index.scss');

Backbone.ajax = require('./modules/main/ajax');

var store = require('./modules/main/store');
var SessionModel = require('./modules/main/models/session');
var UserModel = require('./modules/user/models/user');
var MainRouter = require('./modules/main/router');
var UserRouter = require('./modules/user/router');
var session = new SessionModel();
var user = new UserModel();

store
  .setUser(user)
  .setSession(session)
  .setRouters({
    main: new MainRouter(),
    user: new UserRouter()
  });

user.listenTo(session, 'change:token', function () {
  console.log('session event change:token');
  this.fetch();
});

user.listenTo(session, 'error destroy', function (event) {
  console.log('session event', event);
  this.clear();
});

user.on('change:isAuth', function () {
  if (this.get('isAuth')) {
    store.getRouter('main').navigate('', { trigger: true });
  }
});

session.listenTo(user, 'error', function () {
  this.clear();
});

user.fetch();

Backbone.history.start();
