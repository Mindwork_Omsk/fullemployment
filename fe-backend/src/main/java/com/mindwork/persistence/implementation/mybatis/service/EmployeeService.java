package com.mindwork.persistence.implementation.mybatis.service;

import com.mindwork.persistence.dao.EmployeeDAO;
import com.mindwork.persistence.implementation.mybatis.SqlSessionFactoryProvider;
import com.mindwork.persistence.implementation.mybatis.mapper.EmployeeMapper;
import com.mindwork.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
@Slf4j
public class EmployeeService implements EmployeeDAO {
    private static final EmployeeDAO INSTANCE = new EmployeeService();

    public static EmployeeDAO getInstance() {
        return INSTANCE;
    }

    private EmployeeService() {

    }

    private SqlSession getSqlSession() {
        return SqlSessionFactoryProvider.getSqlSessionFactory().openSession();
    }

    @Override
    public Employee getEmployeeById(Integer id) {
        Employee employee;

        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employee = employeeMapper.getEmployeeById(id);
        }

        return employee;
    }

    @Override
    public Employee getEmployeeByLogin(String login) {
        Employee employee;

        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employee = employeeMapper.getEmployeeByLogin(login);
        }

        return employee;
    }

    @Override
    public List<Employee> getEmployees() {
        List<Employee> employees;

        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employees = employeeMapper.getEmployees();
        }

        return employees;
    }

    @Override
    public void updateEmployee(Employee employee) {
        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employeeMapper.updateEmployee(employee);

            sqlSession.commit();

            log.info("UPDATED ~ {}", employee.toString());
        }
    }

    @Override
    public void insertEmployee(Employee employee) {
        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employeeMapper.insertEmployee(employee);

            sqlSession.commit();

            log.info("INSERTED ~ {}", employee.toString());
        }
    }

    @Override
    public void deleteEmployee(Integer id) {
        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employeeMapper.deleteEmployee(id);

            sqlSession.commit();

            log.info("DELETED EMPLOYEE ~ {}", id);
        }
    }

    @Override
    public void deleteAll() {
        try (SqlSession sqlSession = getSqlSession()) {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            employeeMapper.deleteAll();

            sqlSession.commit();
        }

        log.info("DELETED ALL EMPLOYEE");
    }
}



