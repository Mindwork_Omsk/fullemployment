﻿USE full_employment;

DELETE FROM user;

INSERT INTO user VALUES (NULL, "John1", "john@mail.com", "somepass");
INSERT INTO user VALUES (NULL, "Jane1", "jane@mail.com", "somepass");
INSERT INTO user VALUES (NULL, "Mark1", "mark@mail.com", "somepass");
INSERT INTO user VALUES (NULL, "Dave1", "dave@mail.com", "somepass");
INSERT INTO user VALUES (NULL, "Lockheed Martin1", "LHM@mail.com", "somepass");
INSERT INTO user VALUES (NULL, "Boeing1", "boeing@mail.com", "somepass");
INSERT INTO user VALUES (NULL, "Airbus1", "airbus@mail.com", "somepass");

DELETE FROM employee;

INSERT INTO employee VALUES (NULL, "John", "", "", TRUE, 01/01/2011, 01/01/2011, (SELECT id FROM user u WHERE u.Login = "John1"));
INSERT INTO employee VALUES (NULL, "Jane", "", "", TRUE, 01/01/2012, 01/01/2012, (SELECT id FROM user u WHERE u.Login = "Jane1"));
INSERT INTO employee VALUES (NULL, "Mark", "", "", TRUE, 01/01/2013, 01/01/2013, (SELECT id FROM user u WHERE u.Login = "Mark1"));
INSERT INTO employee VALUES (NULL, "Dave", "", "", TRUE, 01/01/2015, 01/01/2015, (SELECT id FROM user u WHERE u.Login = "Dave1"));

DELETE from employer;

INSERT INTO employer VALUES (NULL, "Lockheed Martin", "", NULL, TRUE, (SELECT id FROM user u WHERE u.Login = "Lockheed Martin1"));
INSERT INTO employer VALUES (NULL, "Boeing", "", NULL, TRUE, (SELECT id FROM user u WHERE u.Login = "Boeing1"));
INSERT INTO employer VALUES (NULL, "Airbus", "", NULL, TRUE, (SELECT id FROM user u WHERE u.Login = "Airbus1"));

DELETE FROM user_relation;

INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "John1"), 'employee', (SELECT id FROM employee e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "John1")));
INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "Jane1"), 'employee', (SELECT id FROM employee e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "Jane1")));
INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "Mark1"), 'employee', (SELECT id FROM employee e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "Mark1")));
INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "Dave1"), 'employee', (SELECT id FROM employee e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "Dave1")));
INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "Lockheed Martin1"), 'employer', (SELECT id FROM employer e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "Lockheed Martin1")));
INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "Boeing1"), 'employer', (SELECT id FROM employer e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "Boeing1")));
INSERT INTO user_relation VALUES (NULL, (SELECT id FROM user u WHERE u.Login = "Airbus1"), 'employer', (SELECT id FROM employer e WHERE e.UserId = (SELECT id FROM user u WHERE u.Login = "Airbus1")));


