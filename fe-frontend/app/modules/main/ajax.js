'use strict';

var store = require('./store');

module.exports = function (request) {
  console.log('ajax request', request);

  var token = store.getToken();
  var defer = $.Deferred();

  var xhr = {};

  defer.promise(xhr);

  xhr.done(function (resp) {
    request.success(resp);
  });

  xhr.fail(function (textStatus) {
    request.error({ description: 'fake ajax' }, textStatus, 'fake ajax');
  });

  _.defaults(request.headers, { 'X-TOKEN': token });

  if (request.type === 'GET' && request.url === '/api/v1/account') {
    setTimeout(function () {
      if (_.isEmpty(token)) {
        defer.reject('User not auth');
      } else {
        defer.resolve({ user_name: 'Test User' });
      }
    }, 1000);
  }

  if (request.type === 'POST' && request.url === '/api/v1/session') {
    setTimeout(function () {
      var data;

      try {
        data = JSON.parse(request.data);
      } catch (e) {
        console.log('cannot parse data');
      }

      if (data.login === 'test' && data.password === 'test') {
        defer.resolve({ token: 'token' + _.random(1, 1000) });
      } else {
        defer.reject('Session not created');
      }
    }, 1000);
  }

  if (request.type === 'DELETE' && request.url === '/api/v1/session') {
    setTimeout(function () {
      if (_.isEmpty(token)) {
        defer.reject('Session not destroyed');
      } else {
        defer.resolve({ status: 0, statusString: 'Session destroyed' });
      }
    }, 1000);
  }

  return xhr;
};
