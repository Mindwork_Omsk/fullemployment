package com.mindwork.persistence.dao;

import com.mindwork.model.Skill;

import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
public interface SkillDAO {
    Skill getSkillById(Integer id);

    List<Skill> getSkills();

    void insertSkill(Skill skill);
}
