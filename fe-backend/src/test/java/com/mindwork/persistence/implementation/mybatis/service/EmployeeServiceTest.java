package com.mindwork.persistence.implementation.mybatis.service;

import com.mindwork.persistence.dao.EmployeeDAO;
import com.mindwork.model.Employee;
import com.mindwork.persistence.implementation.TestData;
import org.junit.*;

import java.util.List;

/**
 * Created by Mindwork on 09.02.16.
 */
public class EmployeeServiceTest {
    private static EmployeeDAO employeeDAO = EmployeeService.getInstance();
    private Employee employee;

    @Before
    public void init() {
        employee = TestData.getEmployee();
    }

    @Test
    public void testGetEmployeeById() {
        employeeDAO.insertEmployee(employee);

        Employee insertedEmployee = employeeDAO.getEmployeeById(employee.getId());

        Assert.assertNotNull(insertedEmployee );
    }

    @Test
    public void testGetEmployees() {
        employeeDAO.insertEmployee(employee);
        employeeDAO.insertEmployee(employee);

        List<Employee> employees = employeeDAO.getEmployees();

        Assert.assertNotNull(employees);
    }

    @Test
    public void testInsertEmployee() {
        employeeDAO.insertEmployee(employee);

        Employee insertedEmployee = employeeDAO.getEmployeeById(employee.getId());

        Assert.assertEquals(employee, insertedEmployee);
    }

    @Test
    public void testDeleteEmployee() {
        boolean success;

        employeeDAO.insertEmployee(employee);

        success = (employee.getId() != null);

        employeeDAO.deleteEmployee(employee.getId());
        Employee deletedEmployee = employeeDAO.getEmployeeById(employee.getId());

        success = success && (deletedEmployee == null);

        Assert.assertTrue(success);
    }

    @Test
    public void testUpdateEmployee() {
        employeeDAO.insertEmployee(employee);

        employee.setSurname("wololo");

        employeeDAO.updateEmployee(employee);

        Employee updatedEmployee = employeeDAO.getEmployeeById(employee.getId());

        Assert.assertTrue(employee.getSurname().equals(updatedEmployee.getSurname()));
    }

    @AfterClass
    public static void afterClass() {
        employeeDAO.deleteAll();
    }
}
