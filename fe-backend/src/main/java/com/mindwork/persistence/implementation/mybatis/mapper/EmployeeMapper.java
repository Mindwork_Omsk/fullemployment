package com.mindwork.persistence.implementation.mybatis.mapper;

import com.mindwork.model.Employee;
import java.util.List;

/**
 * Created by Mindwork on 07.02.16.
 */
public interface EmployeeMapper {

    //also with annotations u can like this:
    //@Select("SELECT * FROM employee WHERE id = #{id}")
    Employee getEmployeeById(Integer id);

    Employee getEmployeeByLogin(String login);

    List<Employee> getEmployees();

    void updateEmployee(Employee employee);

    void insertEmployee(Employee employee);

    void deleteEmployee(Integer id);

    void deleteAll();
}
