package com.mindwork.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by ostkri on 23.03.2016.
 */
@ToString
public class Employer extends Entity<Integer> {
    @Getter @Setter private String companyName;
    @Getter @Setter private String address;
    @Getter @Setter private boolean isActiveProfile;
    @Getter @Setter private String contactPersonName;
    @Getter @Setter private String contactPersonSurname;

    public Employer() {
    }
}
