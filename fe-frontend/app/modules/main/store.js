'use strict';

var store = {
  state: {},

  getState: function () {
    return this.state;
  },

  setUser: function (user) {
    this.state.user = user;
    return this;
  },

  setSession: function (session) {
    this.state.session = session;
    return this;
  },

  getSession: function () {
    return this.state.session;
  },

  getToken: function () {
    return this.state.session ? this.state.session.get('token') : '';
  },

  getUser: function () {
    return this.state.user;
  },

  setRouters: function (routers) {
    this.state.routers = routers;
    return this;
  },

  getRouters: function (routers) {
    return this.state.routers;
  },

  getRouter: function (name) {
    return this.state.routers[name];
  }
};

module.exports = store;
