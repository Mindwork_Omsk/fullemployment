'use strict';

var localStorage = require('localStorage');

module.exports = Backbone.Model.extend({
  url: '/api/v1/session',
  defaults: {
    login: '',
    password: '',
    token: ''
  },
  initialize: function () {
    this.set('token', this.getToken() || '');
  },
  parse: function (response) {
    this.setToken(response.token);

    return {
      token: response.token
    };
  },

  clear: function () {
    this.removeToken();
    Backbone.Model.prototype.clear.apply(this, arguments);
  },

  destroy: function () {
    this.removeToken();
    return Backbone.Model.prototype.destroy.apply(this, arguments);
  },

  setToken: function (token) {
    localStorage.setItem('token', token);
  },
  getToken: function () {
    return localStorage.getItem('token');
  },
  removeToken: function () {
    localStorage.removeItem('token');
  }

});
