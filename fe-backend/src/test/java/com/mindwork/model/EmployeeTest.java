package com.mindwork.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by ostkri on 23.03.2016.
 */
public class EmployeeTest {

    Employee employee;

    @Before
    public void init() {
        employee = new Employee();
        employee.setName("vasya");
        employee.setSurname("konoplev");
    }

    @Test
    public void testToString() {
        employee.setName("fedya");
        Assert.assertNotNull(employee.toString());
    }

    @Test
    public void testGetName() {
        Assert.assertNotNull(employee.getName());
    }
}
