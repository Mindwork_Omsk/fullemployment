package com.mindwork.app.properties;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by Mindwork on 18.02.16.
 */
public class PropertyUtilTest {

    @Test
    public void testGetProperties() {
        Properties properties = PropertyUtil.getProperties("config/server_config.properties");

        Assert.assertNotNull(properties);
    }

    @Test(expected = RuntimeException.class)
    public void testGetNotExistProperties() {
        Properties properties = PropertyUtil.getProperties("wakawaka");

        Assert.assertNull(properties.getProperty("waka"));
    }
}
