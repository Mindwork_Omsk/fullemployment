package com.mindwork.persistence.implementation.mybatis.service;

import com.mindwork.model.User;
import com.mindwork.persistence.dao.UserDAO;
import com.mindwork.persistence.implementation.mybatis.SqlSessionFactoryProvider;
import com.mindwork.persistence.implementation.mybatis.mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;

/**
 * Created by Mindwork on 02.04.2016.
 */
public class UserService implements UserDAO {
    private static final UserDAO INSTANCE = new UserService();

    public static UserDAO getInstance() {
        return INSTANCE;
    }

    private UserService() {

    }

    private SqlSession getSqlSession() {
        return SqlSessionFactoryProvider.getSqlSessionFactory().openSession();
    }

    @Override
    public User getUserByLogin(String login) {
        User user;

        try (SqlSession sqlSession = getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            user = userMapper.getUserByLogin(login);
        }

        return user;
    }
}
