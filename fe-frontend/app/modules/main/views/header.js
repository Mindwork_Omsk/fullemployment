'use strict';

require('assets/images/logo.svg');

module.exports = Backbone.View.extend({
  template:  require('../templates/header.hbs'),

  initialize: function () {
    this.listenTo(this.model, 'change', this.render);
  },

  render: function () {
    this.$el.html(this.template({ user: this.model.toJSON() }));
    return this;
  }
});
