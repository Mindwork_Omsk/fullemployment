package com.mindwork.app.properties;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Mindwork on 18.02.16.
 */
public class MessageSourceTest {

    @Test
    public void testGetMessage() {
        String propertyName = "rest.error.userNotFound";
        String message = MessageSource.getInstance().getMessage(propertyName);

        Assert.assertNotNull(message);
    }

    @Test
    public void testGetNotExistMessage() {
        String propertyName = "rest.error.EmployeeResource.userNotFound12312313213";
        String message = MessageSource.getInstance().getMessage(propertyName);

        Assert.assertNull(message);
    }
}
