For comfortable development you can:
 - install plugin for Lombok https://github.com/mplushnikov/lombok-intellij-plugin

How to run this app:

1. Configure server_config.properties, business.properties
2. Do mvn clean package on project root
3. Run by command line: ----todo----


Assemble frontend via webpack:

1. npm i --save-dev - download all node_modules what declared in package.json
2. webpack - jobs done