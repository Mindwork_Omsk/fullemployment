'use strict';

var webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var NODE_ENV = process.env.NODE_ENV || 'development';
var extractCSS = new ExtractTextPlugin('css/styles.css');
var autoprefixer = require('autoprefixer');

module.exports = {
  context: __dirname + '/app',

  entry: {
    index: [
      // 'webpack-dev-server/client?http://localhost:8082',
       'bootstrap-loader/extractStyles', // Bootstrap 3 in css/styles.css
      './index'
    ]
  },

  watchOptions: {
    aggregateTimeout: 300
  },

  devtool: NODE_ENV === 'development' ? 'source-map' : null,

  externals: {
    localStorage: 'window.localStorage'
  },

  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
      LANG: JSON.stringify('en')
    }),

    new webpack.ResolverPlugin([
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('bower.json', ['main']),
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('.bower.json', ['main'])
    ], ['normal', 'loader']),

    new webpack.ProvidePlugin({
      $:      'jquery',
      jQuery: 'jquery',
      _: 'underscore',
      Backbone: 'backbone',
    }),

    extractCSS,

    new HtmlWebpackPlugin({
      template: './assets/layouts/index.html'
    }),

    new webpack.NoErrorsPlugin()
  ],

  output: {
    path: path.join(__dirname, 'dist'),

    filename: 'js/[name].js'
  },
  resolve: {
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js'],
    root: [
      path.resolve('./app')
    ]
  },
  resolveLoader: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js']
  },

  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: extractCSS.extract(['css?sourceMap', 'postcss', 'sass?sourceMap'])
      },
      {
        test: /\.css$/,
        loader: extractCSS.extract(['css', 'postcss'])
      },
      {
        test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
        include: /\/node_modules\/bootstrap-sass\//,
        loader: 'file?name=[1].[ext]&regExp=node_modules/bootstrap-sass/(.*)'
      },
      {
        test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
        exclude: [/\/node_modules\/bootstrap-sass\//],
        loader: 'file?name=[path][name].[ext]'
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars'
      },
      {
        test: /\.html$/,
        loader: 'html'
      },

      // Bootstrap 3
      {
        test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
        loader: 'imports?jQuery=jquery'
      }
    ]
  },

  postcss: [
    autoprefixer({ browsers: ['last 2 versions'] })
  ],

  devServer: {
    host: 'localhost',
    port: 8082,
    contentBase: path.resolve(__dirname, 'dist')
  }
};

if (NODE_ENV === 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      },
      mangle: {
        except: ['$super', '$', 'exports', 'require']
      }
    })
  );
}
