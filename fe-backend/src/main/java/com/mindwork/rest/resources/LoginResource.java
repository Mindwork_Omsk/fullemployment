package com.mindwork.rest.resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mindwork.model.User;
import com.mindwork.persistence.implementation.mybatis.service.UserService;
import com.mindwork.rest.response.AppResponse;
import com.mindwork.rest.response.AppResponseStatus;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Slf4j
@Path("/api/v1/fe/login")
public class LoginResource {

    private final Gson gson = new Gson();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(String body) {
        Response response;
        User user;

        JsonElement jsonElement = new JsonParser().parse(body);
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        JsonElement loginElem = jsonObject.get("login");
        JsonElement passwordElem = jsonObject.get("password");

        if (loginElem != null && passwordElem != null) {
            String login = loginElem.getAsString();
            String password = passwordElem.getAsString();

            user = UserService.getInstance().getUserByLogin(login);

            if (user != null) {
                if (user.getPass().equals(password)) {
                    response = Response
                            .status(Response.Status.OK)
                            .entity(gson.toJson(AppResponse.createResponseWithGUID(AppResponseStatus.SUCCESS_LOGIN)))
                            .build();

                    //todo create session here, save token-user relation
                } else {
                    response = Response
                            .status(Response.Status.UNAUTHORIZED)
                            .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_AUTHENTICATION)))
                            .build();
                }

            } else {
                response = Response
                        .status(Response.Status.UNAUTHORIZED)
                        .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_LOGIN_NOT_FOUND)))
                        .build();
            }
        } else {
            response = Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_LOGIN_OR_PASSWORD_ABSENT)))
                    .build();
        }

        return response;
    }
}
