package com.mindwork.persistence.dao;

import com.mindwork.model.Employee;
import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
public interface EmployeeDAO {
    Employee getEmployeeById(Integer id);

    Employee getEmployeeByLogin(String login);

    List<Employee> getEmployees();

    void updateEmployee(Employee employee);

    void insertEmployee(Employee employee);

    void deleteEmployee(Integer id);

    void deleteAll();
}
