package com.mindwork.rest.resources;

import com.google.common.collect.Sets;
import org.reflections.Reflections;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

/**
 * Created by Mindwork on 01.03.16.
 */
@Path("/api/v1/fe")
public class BaseResource {

    private Set<String> getURLPaths() {
        Set<String> reflectionsStringSet = Sets.newTreeSet();
        Reflections reflections = new Reflections("com.mindwork.rest.resources");
        Set<Class<?>> paths = reflections.getTypesAnnotatedWith(Path.class);

        for (Class<?> clazz : paths) {
            Path path = clazz.getAnnotation(Path.class);

            reflectionsStringSet.add(path.value());
        }

        return reflectionsStringSet;
    }

    private String getFormattedPaths(Set<String> urlPaths) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String urlPath : urlPaths) {
            stringBuilder.append("<link href=\"" + urlPath + "\">" + "\r\n");
        }

        return stringBuilder.toString();
    }

    @OPTIONS
    @Produces(MediaType.TEXT_PLAIN)
    public Response getOptions() {
        Set<String> urlPaths = getURLPaths();

        return Response.ok(getFormattedPaths(urlPaths)).build();
    }
}
