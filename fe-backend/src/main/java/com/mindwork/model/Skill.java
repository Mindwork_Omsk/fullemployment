package com.mindwork.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by ostkri on 23.03.2016.
 */
@ToString
public class Skill extends Entity<Integer> {
    @Getter @Setter private String name;

    public Skill() {
    }
}
