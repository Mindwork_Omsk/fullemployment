'use strict';

module.exports = Backbone.View.extend({
  template: require('../templates/signup.hbs'),

  events: {
    'click .nav-tabs a': 'changeTab'
  },

  render: function (param) {
    this.$el.html(this.template({
      isEmployer: !param || param === 'employer',
      isApplicant: param === 'applicant'
    }));
    return this;
  },

  changeTab: function (event) {
    var href = $(event.currentTarget).prop('href');
    var route = href.replace(/^(.*)#/, '');

    this.trigger('signup:changeTab', route);
  }
});
