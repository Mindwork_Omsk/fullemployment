package com.mindwork.rest.response;

/**
 * Created by Mindwork on 19.02.16.
 */
public enum AppResponseStatus {

    SUCCESS_REGISTRATION(0, "rest.success.registration"),
    SUCCESS_LOGIN(5, "rest.success.login"),
    SUCCESS_LOGOUT(10, "rest.success.logout"),
    SUCCESS_UNREGISTRATION(15, "rest.success.unregistration"),
    ERROR_WRONG_DATAFORMAT(20, "rest.error.wrongDataFormat"),
    ERROR_REGISTRATION(25, "rest.error.registration"),
    ERROR_SOMETHING_WENT_WRONG(26, "rest.error.somethingWentWrong"),
    ERROR_USER_NOT_FOUND(30, "rest.error.userNotFound"),
    ERROR_LOGIN_NOT_FOUND(35, "rest.error.loginNotFound"),
    ERROR_LOGIN_OR_PASSWORD_ABSENT(36, "rest.error.loginOrPasswordAbsent"),
    ERROR_AUTHENTICATION(40, "rest.error.wrongPassword");

    public Integer statusCode;
    public String statusString;

    AppResponseStatus(Integer status, String statusString) {
        this.statusCode = status;
        this.statusString = statusString;
    }
}
