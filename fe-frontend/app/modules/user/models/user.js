'use strict';

module.exports = Backbone.Model.extend({
  url: '/api/v1/account',
  defaults: {
    userName: '',
    isAuth: false
  },

  parse: function (response) {
    console.log('user parse', response);
    return {
      userName: response.user_name,
      isAuth: !_.isEmpty(response.user_name)
    };
  }
});
