'use strict';

module.exports = Backbone.View.extend({
  template:  require('../templates/index.hbs'),

  render: function () {
    this.$el.html(this.template({ test: 1 }));
    return this;
  }
});
