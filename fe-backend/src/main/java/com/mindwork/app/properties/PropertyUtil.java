package com.mindwork.app.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Mindwork on 18.02.16.
 */
public class PropertyUtil {
    public static Properties getProperties(String resource) {
        Properties properties = new Properties();

        try (InputStream is = PropertyUtil.class.getClassLoader().getResourceAsStream(resource)) {
            properties.load(is);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        return properties;
    }
}
