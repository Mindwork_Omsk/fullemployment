package com.mindwork.model;

/**
 * Created by Mindwork on 04.05.2016.
 */
public enum UserRole {

    EMPLOYEE("employee"),
    EMPLOYER("employer");

    private String userRole;

    UserRole(String userRole) {
        this.userRole = userRole;
    }
}
