package com.mindwork.persistence.dao;

import com.mindwork.model.Employer;

import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
public interface EmployerDAO {
    Employer getEmployerById(Integer id);

    Employer getEmployerByLogin(String login);

    List<Employer> getEmployers();

    void updateEmployer(Employer employer);

    void insertEmployer(Employer employer);

    void deleteEmployer(Integer id);

    void deleteAll();
}
