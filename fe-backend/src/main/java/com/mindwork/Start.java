package com.mindwork;

import com.mindwork.app.properties.ServerConfigService;
import com.mindwork.rest.MyApplication;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Created by Mindwork on 07.02.16.
 */
public class Start {
    public static void main(String[] args) {
        ServerConfigService serverConfig = ServerConfigService.getInstance();

        //todo move into config
        URI baseUri = UriBuilder
                .fromUri(serverConfig.getStringProperty("http.server.address"))
                .port(serverConfig.getIntegerProperty("http.server.port")).build();
        MyApplication config = new MyApplication();
        Server server = JettyHttpContainerFactory.createServer(baseUri, config);
    }
}
