package com.mindwork.persistence.dao;

import com.mindwork.model.Employee;
import com.mindwork.model.JobOffer;

import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
public interface JobOfferDAO {
    Employee getJobOfferById(Integer id);

    List<Employee> getJobOffers();

    void updateJobOffer(JobOffer jobOffer);

    void insertJobOffer(JobOffer jobOffer);

    void deleteJobOffer(Integer id);
}
