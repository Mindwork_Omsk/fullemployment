package com.mindwork.persistence.implementation.mybatis;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Mindwork on 13.02.16.
 */
public class SqlSessionFactoryProvider {
    private static SqlSessionFactory sqlSessionFactory;

    private static synchronized void initSqlSessionFactory() {
        InputStream inputStream;

        try {
            //todo move into config
            inputStream = Resources.getResourceAsStream("mybatis/myBatis_config.xml");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    private SqlSessionFactoryProvider() {
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        synchronized (SqlSessionFactoryProvider.class) {
            if (sqlSessionFactory == null) {
                initSqlSessionFactory();
            }
        }

        return sqlSessionFactory;
    }
}
