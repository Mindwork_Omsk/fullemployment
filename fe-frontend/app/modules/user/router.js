'use strict';

var loginUserController = require('./controllers/login');
var logoutUserController = require('./controllers/logout');
var signupUserController = require('./controllers/signup');

module.exports = Backbone.Router.extend({
  routes: {
    'user/login': loginUserController,
    'user/logout': logoutUserController,
    'user/signup': signupUserController,
    'user/signup/:params': signupUserController
  }
});
