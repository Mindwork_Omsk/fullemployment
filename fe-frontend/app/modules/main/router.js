'use strict';

var indexController = require('./controllers');
var privateController = require('./controllers/private');
var HeaderView = require('./views/header');
var store = require('../main/store');

module.exports = Backbone.Router.extend({
  routes: {
    '': indexController,
    'private': privateController
  },

  initialize: function () {
    new HeaderView({
      el: $('.header'),
      model: store.getUser()
    }).render();
  }
});
