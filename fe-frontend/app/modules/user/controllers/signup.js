'use strict';

var SignupView = require('../views/signup');
var store = require('../../main/store');

module.exports = function (param) {
  var router = this;
  var user = store.getUser();

  if (user.get('isAuth')) {
    this.navigate('', { trigger: true });
  }

  var signupView = new SignupView();

  signupView.on('signup:changeTab', function (route) {
    router.navigate(route, { replace: true });
  });

  $('.page').empty().append(signupView.render(param).$el);
};
