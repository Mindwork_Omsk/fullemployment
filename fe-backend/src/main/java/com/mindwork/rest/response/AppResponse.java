package com.mindwork.rest.response;

import com.mindwork.app.properties.MessageSource;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Mindwork on 19.02.16.
 */
public class AppResponse {
    @Getter @Setter private Integer statusCode;
    @Getter @Setter private String statusString;
    @Getter @Setter private UUID guid;

    public AppResponse() {
    }

    public static AppResponse createResponse(AppResponseStatus status) {
        AppResponse response = new AppResponse();

        response.setStatusCode(status.statusCode);
        response.setStatusString(MessageSource.getInstance().getMessage(status.statusString));

        return response;
    }

    public static AppResponse createResponseWithGUID(AppResponseStatus status) {
        AppResponse response = new AppResponse();

        response.setStatusCode(status.statusCode);
        response.setStatusString(MessageSource.getInstance().getMessage(status.statusString));
        response.setGuid(UUID.randomUUID());

        return response;
    }
}
