package com.mindwork.app.properties;

import java.util.Properties;

/**
 * Created by Mindwork on 18.02.16.
 */
public class ServerConfigService {
    private static final ServerConfigService INSTANCE = new ServerConfigService();
    private static Properties PROPERTIES;
    private String resource = "config/server_config.properties";

    private ServerConfigService() {
        PROPERTIES = PropertyUtil.getProperties(resource);
    }

    public static ServerConfigService getInstance() {
        return INSTANCE;
    }

    public String getStringProperty(String propertyName) {
        return PROPERTIES.getProperty(propertyName);
    }

    public Integer getIntegerProperty(String propertyName) {
        return Integer.valueOf(PROPERTIES.getProperty(propertyName));
    }
}
