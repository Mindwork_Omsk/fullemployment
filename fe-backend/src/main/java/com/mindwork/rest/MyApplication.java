package com.mindwork.rest;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by Mindwork on 13.02.16.
 */
public class MyApplication extends ResourceConfig {

    public MyApplication() {
        packages("com.mindwork.rest.resources");
    }
}

