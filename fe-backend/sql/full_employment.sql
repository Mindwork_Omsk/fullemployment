--
-- ������ ������������ Devart dbForge Studio for MySQL, ������ 7.0.54.0
-- �������� �������� ��������: http://www.devart.com/ru/dbforge/mysql/studio
-- ���� �������: 05.05.2016 0:08:10
-- ������ �������: 5.7.12-log
-- ������ �������: 4.1
--


-- 
-- ���������� ������� ������
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- ���������� ����� SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- ��������� ���� ������ �� ���������
--
USE full_employment;

--
-- �������� ��� ������� employee
--
DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) DEFAULT NULL,
  Patronymic VARCHAR(255) DEFAULT NULL,
  Surname VARCHAR(255) DEFAULT NULL,
  IsActiveProfile TINYINT(1) DEFAULT NULL,
  CreationDate DATE DEFAULT NULL,
  ModificationDate DATE DEFAULT NULL,
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 27
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� employer
--
DROP TABLE IF EXISTS employer;
CREATE TABLE employer (
  Id INT(11) NOT NULL AUTO_INCREMENT,
  CompanyName VARCHAR(50) DEFAULT NULL,
  Address VARCHAR(255) DEFAULT NULL,
  IsActiveProfile TINYINT(1) DEFAULT NULL,
  ContactPersonName VARCHAR(50) DEFAULT NULL,
  ContactPersonSurname VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� skill
--
DROP TABLE IF EXISTS skill;
CREATE TABLE skill (
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� user
--
DROP TABLE IF EXISTS user;
CREATE TABLE user (
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Login VARCHAR(50) NOT NULL,
  Email VARCHAR(50) NOT NULL,
  Pass VARCHAR(50) NOT NULL,
  UserRole ENUM('employee','employer') NOT NULL,
  UserEntityId INT(11) NOT NULL,
  PRIMARY KEY (Id),
  UNIQUE INDEX UK_user_Email (Email),
  UNIQUE INDEX UK_user_Login (Login)
)
ENGINE = INNODB
AUTO_INCREMENT = 10
AVG_ROW_LENGTH = 2340
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� employee_skill
--
DROP TABLE IF EXISTS employee_skill;
CREATE TABLE employee_skill (
  Id INT(11) NOT NULL AUTO_INCREMENT,
  EmployeeId INT(11) NOT NULL,
  SkillId INT(11) NOT NULL,
  Level ENUM('1','2','3','4','5') NOT NULL,
  PRIMARY KEY (Id),
  CONSTRAINT FK_employee_skill_employee_Id FOREIGN KEY (EmployeeId)
    REFERENCES employee(Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_employee_skill_skill_Id FOREIGN KEY (SkillId)
    REFERENCES skill(Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� job_offer
--
DROP TABLE IF EXISTS job_offer;
CREATE TABLE job_offer (
  Id INT(11) NOT NULL AUTO_INCREMENT,
  EmployerId INT(11) NOT NULL,
  EmployeePositionName VARCHAR(255) DEFAULT NULL,
  Salary VARCHAR(255) DEFAULT NULL,
  CreationDate DATETIME DEFAULT NULL,
  ModificationDate DATETIME DEFAULT NULL,
  PRIMARY KEY (Id),
  CONSTRAINT FK_job_offer_employer_Id FOREIGN KEY (EmployerId)
    REFERENCES employer(Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� job_offer_candidate
--
DROP TABLE IF EXISTS job_offer_candidate;
CREATE TABLE job_offer_candidate (
  Id INT(11) NOT NULL,
  JobOfferId INT(11) NOT NULL,
  EmployeeId INT(11) NOT NULL,
  PRIMARY KEY (Id),
  CONSTRAINT FK_job_offer_candidate_employee_Id FOREIGN KEY (EmployeeId)
    REFERENCES employee(Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_job_offer_candidate_job_offer_Id FOREIGN KEY (JobOfferId)
    REFERENCES job_offer(Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- �������� ��� ������� job_offer_skill
--
DROP TABLE IF EXISTS job_offer_skill;
CREATE TABLE job_offer_skill (
  Id INT(11) NOT NULL,
  SkillId INT(11) NOT NULL,
  JobOfferId INT(11) NOT NULL,
  Level ENUM('1','2','3','4','5') NOT NULL,
  PRIMARY KEY (Id),
  CONSTRAINT FK_job_offer_skill_job_offer_Id FOREIGN KEY (JobOfferId)
    REFERENCES job_offer(Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_job_offer_skill_skill_Id FOREIGN KEY (SkillId)
    REFERENCES skill(Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

-- 
-- ����� ������ ��� ������� employee
--
INSERT INTO employee VALUES
(23, 'John', '', '', 1, '2016-05-04', '2016-05-04'),
(24, 'Jane', '', '', 1, '2016-05-04', '2016-05-04'),
(25, 'Mark', '', '', 1, '2016-05-04', '2016-05-04'),
(26, 'Dave', '', '', 1, '2016-05-04', '2016-05-04');

-- 
-- ����� ������ ��� ������� employer
--
INSERT INTO employer VALUES
(4, 'Lockheed Martin', '', 1, NULL, NULL),
(5, 'Boeing', '', 1, NULL, NULL),
(6, 'Airbus', '', 1, NULL, NULL);

-- 
-- ����� ������ ��� ������� skill
--
INSERT INTO skill VALUES
(1, 'java'),
(2, 'ajax');

-- 
-- ����� ������ ��� ������� user
--
INSERT INTO user VALUES
(3, 'John1', 'john@mail.com', 'somepass', 'employee', 23),
(4, 'Jane1', 'jane@mail.com', 'somepass', 'employee', 24),
(5, 'Mark1', 'mark@mail.com', 'somepass', 'employee', 25),
(6, 'Dave1', 'dave@mail.com', 'somepass', 'employee', 26),
(7, 'Lockheed Martin1', 'LHM@mail.com', 'somepass', 'employer', 4),
(8, 'Boeing1', 'boeing@mail.com', 'somepass', 'employer', 5),
(9, 'Airbus1', 'airbus@mail.com', 'somepass', 'employer', 6);

-- 
-- ����� ������ ��� ������� employee_skill
--

-- ������� full_employment.employee_skill �� �������� ������

-- 
-- ����� ������ ��� ������� job_offer
--

-- ������� full_employment.job_offer �� �������� ������

-- 
-- ����� ������ ��� ������� job_offer_candidate
--

-- ������� full_employment.job_offer_candidate �� �������� ������

-- 
-- ����� ������ ��� ������� job_offer_skill
--

-- ������� full_employment.job_offer_skill �� �������� ������

-- 
-- ������������ ���������� ����� SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- ��������� ������� ������
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;