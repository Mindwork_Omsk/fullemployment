'use strict';

module.exports = Backbone.View.extend({
  template: require('../templates/login.hbs'),

  events: {
    'submit form': 'login'
  },

  serialize: function() {
    return {
      login: this.$('[name="user_login"]').val(),
      password: this.$('[name="user_password"]').val()
    };
  },

  render: function (param) {
    this.$el.html(this.template(param || {}));
    return this;
  },

  login: function (event) {
    event.preventDefault();
    this.trigger('user:login', this.serialize());
  }
});
