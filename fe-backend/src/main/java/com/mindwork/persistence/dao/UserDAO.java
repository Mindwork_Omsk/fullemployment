package com.mindwork.persistence.dao;

import com.mindwork.model.User;

import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
public interface UserDAO {
    User getUserByLogin(String login);
}
