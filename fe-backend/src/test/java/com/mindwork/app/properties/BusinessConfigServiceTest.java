package com.mindwork.app.properties;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Mindwork on 18.02.16.
 */
public class BusinessConfigServiceTest {
    @Test
    public void getMaxJobs() {
        Assert.assertNotNull(BusinessConfigService.getInstance().getMaxJobs());
    }

    @Test
    public void getMaxEmployerSkills() {
        Assert.assertNotNull(BusinessConfigService.getInstance().getMaxEmployerSkills());
    }

    @Test
    public void getMaxEmployeeSkills() {
        Assert.assertNotNull(BusinessConfigService.getInstance().getMaxEmployeeSkills());
    }

    @Test
    public void getJobExpiration() {
        Assert.assertNotNull(BusinessConfigService.getInstance().getJobExpiration());
    }

    @Test
    public void getEmployeeExpiration() {
        Assert.assertNotNull(BusinessConfigService.getInstance().getEmployeeExpiration());
    }
}
