'use strict';

var store = require('../../main/store');

module.exports = function () {
  var router = this;

  $('.page').empty().html('logout...');

  store.getSession().destroy({
    success: function () {
      console.log('logout user');
      router.navigate('', { trigger: true });
    }
  });
};
