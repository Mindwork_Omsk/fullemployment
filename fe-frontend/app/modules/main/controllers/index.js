'use strict';

var IndexView = require('../views/index');

module.exports = function () {
  $('.page').empty().append((new IndexView()).render().$el);
};
