package com.mindwork.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.Date;

/**
 * Created by ostkri on 23.03.2016.
 */
@ToString
public class JobOffer extends Entity<Integer> {
    @Getter @Setter private Integer employerId;
    @Getter @Setter private String employeePositionName;
    @Getter @Setter private String salary;
    @Getter @Setter private Date creationDate;
    @Getter @Setter private Date modificationDate;

    public JobOffer() {
    }
}
