package com.mindwork.model;

/**
 * Created by Mindwork on 15.02.16.
 */
public abstract class Entity<T> {
    protected T id = null;

    private void setId(T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (!id.equals(employee.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        Object object = id;

        return object.hashCode();
    }
}
