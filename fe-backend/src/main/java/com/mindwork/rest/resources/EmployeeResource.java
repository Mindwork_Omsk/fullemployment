package com.mindwork.rest.resources;

import com.google.gson.*;
import com.mindwork.persistence.dao.EmployeeDAO;
import com.mindwork.persistence.implementation.mybatis.service.EmployeeService;
import com.mindwork.model.Employee;
import com.mindwork.rest.response.AppResponse;
import com.mindwork.rest.response.AppResponseStatus;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Mindwork on 13.02.16.
 */
@Slf4j
@Path("/api/v1/fe/employee")
public class EmployeeResource {
    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-dd-mm").create();
    private static EmployeeDAO dao = EmployeeService.getInstance();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        Response response;

        try {
            List<Employee> employees = dao.getEmployees();

            if (employees.isEmpty()) {
                response = Response
                        .status(Response.Status.NOT_FOUND)
                        .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_USER_NOT_FOUND)))
                        .build();
            } else {
                response = Response
                        .ok(gson.toJson(employees))
                        .build();
            }
        } catch (Exception e) {
            log.error(e.getMessage());

            response = Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_SOMETHING_WENT_WRONG)))
                    .build();
        }

        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{login}")
    public Response getByLogin(@PathParam("login") String login) {
        Response response;

        try {
            Employee employee = dao.getEmployeeByLogin(login);

            if (employee == null) {
                response = Response
                        .status(Response.Status.NOT_FOUND)
                        .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_USER_NOT_FOUND)))
                        .build();
            } else {
                response = Response
                        .ok(gson.toJson(employee))
                        .build();
            }
        } catch (Exception e) {
            log.error(e.getMessage());

            response = Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_SOMETHING_WENT_WRONG)))
                    .build();
        }

        return response;
    }

    //incorrect, remove later
/*
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{login}/unregister")
    public Response unregisterEmployee(@PathParam("login") String login, String body) {
        //todo it can be more easily, think more
        JsonElement jsonElement = new JsonParser().parse(body);
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        //todo String password = jsonObject.get("password").getAsString();
        Employee employee = dao.getEmployeeByLogin(login);
        Response response;

        if (employee == null) {
            response = Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_USER_NOT_FOUND)))
                    .build();
        } else {
            try {
                employee.setIsActiveProfile(false);

                dao.updateEmployee(employee);

                response = Response
                        .ok(gson.toJson(AppResponse.createResponse(AppResponseStatus.SUCCESS_UNREGISTRATION)))
                        .build();
            } catch (AuthenticationException e) {
                response = Response
                        .status(Response.Status.UNAUTHORIZED)
                        .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_AUTHENTICATION)))
                        .build();
            } catch (Exception e) {
                response = Response
                        .status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(gson.toJson(AppResponse.createResponse(AppResponseStatus.ERROR_SOMETHING_WENT_WRONG)))
                        .build();
            }
        }

        return response;
    }
*/
}
