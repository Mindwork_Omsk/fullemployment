package com.mindwork.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.Date;

@ToString
public class Employee extends Entity<Integer> {
    @Getter @Setter private String name;
    @Getter @Setter private String patronymic;
    @Getter @Setter private String surname;
    @Getter @Setter private Boolean isActiveProfile;
    @Getter @Setter private Date creationDate;
    @Getter @Setter private Date modificationDate;

    public Employee() {
    }
}
