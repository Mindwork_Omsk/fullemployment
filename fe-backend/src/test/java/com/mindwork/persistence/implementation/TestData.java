package com.mindwork.persistence.implementation;

import com.mindwork.model.Employee;

import java.util.Date;

/**
 * Created by ostkri on 23.03.2016.
 */
public class TestData {

    public static Employee getEmployee() {
        Employee employee = new Employee();

        employee.setName("java name");
        employee.setPatronymic("");
        employee.setSurname("");
        employee.setIsActiveProfile(true);
        employee.setCreationDate(new Date(System.currentTimeMillis()));
        employee.setModificationDate(new Date(System.currentTimeMillis()));

        return employee;
    }
}
