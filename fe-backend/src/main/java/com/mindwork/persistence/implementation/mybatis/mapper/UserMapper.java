package com.mindwork.persistence.implementation.mybatis.mapper;

import com.mindwork.model.User;

/**
 * Created by Mindwork on 02.04.2016.
 */
public interface UserMapper {

    User getUserByLogin(String login);
}
