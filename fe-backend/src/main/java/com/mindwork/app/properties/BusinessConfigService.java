package com.mindwork.app.properties;

import java.util.Properties;

/**
 * Created by Mindwork on 18.02.16.
 */
public class BusinessConfigService {
    private static final BusinessConfigService INSTANCE = new BusinessConfigService();
    private static Properties PROPERTIES;
    private String resource = "config/business.properties";

    private Integer maxJobs;
    private Integer maxEmployerSkills;
    private Integer maxEmployeeSkills;
    private Integer jobExpiration;
    private Integer employeeExpiration;


    private BusinessConfigService() {
        PROPERTIES = PropertyUtil.getProperties(resource);

        maxJobs = Integer.valueOf(PROPERTIES.getProperty("max_jobs"));
        maxEmployerSkills = Integer.valueOf(PROPERTIES.getProperty("max_employer_skills"));
        maxEmployeeSkills = Integer.valueOf(PROPERTIES.getProperty("max_employee_skills"));
        jobExpiration = Integer.valueOf(PROPERTIES.getProperty("job_expiration"));
        employeeExpiration = Integer.valueOf(PROPERTIES.getProperty("employee_expiration"));
    }

    public static BusinessConfigService getInstance() {
        return INSTANCE;
    }

    public Integer getMaxJobs() {
        return maxJobs;
    }

    public Integer getMaxEmployerSkills() {
        return maxEmployerSkills;
    }

    public Integer getMaxEmployeeSkills() {
        return maxEmployeeSkills;
    }

    public Integer getJobExpiration() {
        return jobExpiration;
    }

    public Integer getEmployeeExpiration() {
        return employeeExpiration;
    }
}
