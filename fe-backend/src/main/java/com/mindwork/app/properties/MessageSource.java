package com.mindwork.app.properties;

import java.util.Properties;

/**
 * Created by Mindwork on 17.02.16.
 */
//todo подумать над тем как не дублировать подобные классы
public class MessageSource {
    private static final MessageSource INSTANCE = new MessageSource();
    private static Properties PROPERTIES;
    private String resource = "messages/en.properties";

    private MessageSource() {
        PROPERTIES = PropertyUtil.getProperties(resource);
    }

    public static MessageSource getInstance() {
        return INSTANCE;
    }

    public String getMessage(String propertyName) {
        return PROPERTIES.getProperty(propertyName);
    }
}
