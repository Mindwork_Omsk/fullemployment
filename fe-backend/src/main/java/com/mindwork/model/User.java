package com.mindwork.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by ostkri on 23.03.2016.
 */
@ToString
public class User extends Entity<Integer> {
    @Getter @Setter private String login;
    @Getter @Setter private String email;
    @Getter @Setter private String pass;
    @Getter @Setter private UserRole userRole;
    @Getter @Setter private Integer userEntityId;

    public User() {
    }
}
