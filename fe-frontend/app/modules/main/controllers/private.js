'use strict';

var store = require('../store');

module.exports = function () {
  if (store.getUser().get('isAuth')) {
    $('.page').empty().append('private page');
  } else {
    this.navigate('user/login', { trigger: true });
  }
};
